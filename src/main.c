#include "gpio_pin_cfg.h"
#include "stm32f103xb.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

volatile uint32_t msTicks = 0;

void Delay(uint32_t dlyTicks){
      uint32_t curTicks;

      curTicks = msTicks;
      while ((msTicks - curTicks) < dlyTicks) ;
}

void send_char(char c){
	while (!(USART2->SR & USART_SR_TXE));
	USART2->DR = c;
}

int __io_putchar(int c)
{
	if (c=='\n')
	        send_char('\r');
    send_char(c);
    return c;
}
int _write(int file,char *ptr, int len)
{
	int DataIdx;
	for(DataIdx= 0; DataIdx< len; DataIdx++)
	{
		__io_putchar(*ptr++);
	}
	return len;
}

int main(void){
	RCC->APB2ENR = RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPCEN
			| RCC_APB2ENR_ADC1EN;
	RCC->APB1ENR = RCC_APB1ENR_USART2EN;

	SysTick_Config(8000);

	gpio_pin_cfg(GPIOA, PA2, gpio_mode_alternate_PP_2MHz);
	gpio_pin_cfg(GPIOA, PA3, gpio_mode_input_floating);

	gpio_pin_cfg(GPIOC, PC0, gpio_mode_output_PP_2MHz);
	gpio_pin_cfg(GPIOC, PC1, gpio_mode_output_PP_2MHz);
	gpio_pin_cfg(GPIOC, PC2, gpio_mode_output_PP_2MHz);

	ADC1->CR2 = ADC_CR2_ADON | ADC_CR2_CONT;
	Delay(10);
	ADC1->SQR3 = 17;
	ADC1->CR2 |= ADC_CR2_ADON;


	USART2->BRR = 8000000/9600;
	USART2->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;

	int cnt = 0;
	uint16_t adc = 0;
	uint8_t size = 3;
	unsigned int index = 0;
	char *my_buffer;
	my_buffer = malloc(sizeof(char) * size);
	memset(my_buffer, 0, sizeof(char) * size);

	while(1){
		cnt++;
		if(ADC1->SR & ADC_SR_EOC){
			ADC1->SR &= ~ADC_SR_EOC;
			adc = ADC1->DR;
		}

		if(cnt > 1000000) {
			printf("Adc = %d (%.3fV)\n", adc, (float)adc * 3.3f / 4096.0f);
			cnt = 0;
		}

		if (USART2->SR & USART_SR_RXNE)	{
				USART2->SR &= ~USART_SR_RXNE;
				char c = USART2->DR;
				my_buffer[index++] = c;

				if(c == '\n' || c == '\r')
				{
					if(strstr(my_buffer, "on1"))
					{
						printf("Power ON\n");
						BB(GPIOC->ODR, PC0) = 1;
					}
					else if(strstr(my_buffer, "off1"))
					{
						printf("Power OFF\n");
						BB(GPIOC->ODR, PC0) = 0;
					}
					else if(strstr(my_buffer, "on2"))
					{
						printf("Power ON\n");
						BB(GPIOC->ODR, PC1) = 1;
					}
					else if(strstr(my_buffer, "off2"))
					{
						printf("Power OFF\n");
						BB(GPIOC->ODR, PC1) = 0;
					}
					else {
						printf("Blad\n");
					}
					memset(my_buffer, 0, sizeof(char) * size);
					index=0;
				}
			}
			//Delay(1000);
	}

} /* main */


/*

__attribute__((interrupt)) void TIM1_UP_IRQHandler(void){
	if (TIM1->SR & TIM_SR_UIF){
		TIM1->SR = ~TIM_SR_UIF;
		BB(GPIOA->ODR, PA5) ^=1;
	}
}
*/
__attribute__((interrupt)) void SysTick_Handler(void)
{
	msTicks++;

}



